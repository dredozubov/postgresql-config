# CHANGELOG

## 0.0.2

* `base` constraints relaxed
* changelog added
* cabal file beautified

## 0.0.1

First version
